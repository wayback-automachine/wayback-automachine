# wayback-automachine

Two scripts to automatically archive websites to [Internet Archive](https://archive.org/)
based on their RSS feed.

## Installation

```
$ sudo apt-get install tidy
$ pip3 install -r requirements.txt
```

## Usage

```
$ ./code/extract.py https://example.com/feed/ | xargs ./code/archive.py
```

## Running inside CI

This repository is configured to regularly run archiving of websites based on RSS feeds in [`list.txt`](./list.txt) file.
The list contains RSS feeds of media outlets in Slovenia.

### Adding to the list

Feel free to make a merge request to add more RSS feeds of media outlets in Slovenia to the list.

### Using with your own list

Fork the repository and change the file to suit your needs. Make sure to configure a
[scheduled CI run](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) at a desired interval.
