#!/usr/bin/env python3

import sys
import time

import requests

SLEEP_BETWEEN_ITEMS = 5  # seconds
SLEEP_BETWEEN_RETRIES = 15  # seconds
REQUEST_TIMEOUT = 5 * 60  # seconds
RETRIES = 15


def urlopen(url):
    for i in range(RETRIES):
        try:
            response = requests.get(
                url,
                headers={
                    'User-Agent': 'archiveorg-backup (https://gitlab.com/mitar/archiveorg-backup)',
                },
                timeout=REQUEST_TIMEOUT,
                allow_redirects=False,
            )
            response.raise_for_status()
            if 300 <= response.status_code < 400:
                # It seems this URL is redirecting.
                return response
            return response
        except Exception as error:
            if isinstance(error, requests.HTTPError) and error.response.status_code in [429, 520]:
                # It seems this URL has already been archived recently, or Internet Archive is under high load.
                # No need to retry at this point then.
                return error.response

            if i == RETRIES - 1:
                raise error
            else:
                print("WARNING: Exception getting '{url}': {error}".format(url=url, error=error), file=sys.stderr)

                # Sleep a bit.
                time.sleep(SLEEP_BETWEEN_RETRIES)


def archive(url):
    failed = 0

    try:
        print("Archiving: {url}".format(url=url), file=sys.stderr)

        # We just wait for request to get through. We do not use the response.
        with urlopen('https://web.archive.org/save/' + url):
            pass

    except KeyboardInterrupt:
        raise
    except Exception as error:
        print("ERROR: Archiving '{url}' failed: {error}".format(url=url, error=error), file=sys.stderr)
        if isinstance(error, requests.HTTPError):
            sys.stderr.write(error.response.text)
        failed += 1

    # Sleep a bit.
    time.sleep(SLEEP_BETWEEN_ITEMS)

    return failed


def main(argv):
    failed = 0

    for url in argv[1:]:
        if not url:
            continue

        failed += archive(url)

    return failed


if __name__ == '__main__':
    failed = main(sys.argv)
    sys.exit(bool(failed))
